package ru.omsu.imit.firsttask;

public class Arithmetic {
    public static void arithmetic(double a, double b, double c){
        System.out.println(a * b * c);
        System.out.println((a + b + c)/3);
        double max = Math.max(c, Math.max(a, b));
        double min = Math.min(c, Math.min(a, b));
        System.out.println(min);
        if(a == max && b == min || a ==min && b == max){
            System.out.println(c);
        }
        else if(b == max && c == min || b ==min && c == max){
            System.out.println(a);
        }else
            System.out.println(b);
        System.out.println(max);
    }

    public static void arithmeticInt(int a, int b, int c){
        System.out.println(a * b * c);
        System.out.println((double) (a + b + c)/3);
        double max = Math.max(c, Math.max(a, b));
        double min = Math.min(c, Math.min(a, b));
        System.out.println(min);
        if(a == max && b == min || a ==min && b == max){
            System.out.println(c);
        }
        else if(b == max && c == min || b ==min && c == max){
            System.out.println(a);
        }else
            System.out.println(b);
        System.out.println(max + "\n");
    }

    public static void main(String[] args) {
        arithmetic(1, 2, 5);
        arithmeticInt(1, 2, 5);

    }

}
