package ru.omsu.imit.firsttask;

import java.math.BigDecimal;

public class Exp {

    public static BigDecimal taylorSeries(double x, double acc) throws Exception{
        if(acc < 0){
            throw new Exception("Accuracy is less then 0");
        }
        BigDecimal sum = BigDecimal.ONE,nextSum = BigDecimal.ONE;
        for(int i = 1;nextSum.abs().compareTo(new BigDecimal(acc)) >= 0 ;++i){
            nextSum = nextSum.multiply(new BigDecimal(x / i));
            sum = sum.add(nextSum);
        }
        return sum;

    }
}
