package ru.omsu.imit.firsttask;

import java.util.Scanner;

public class Tabulation {
    public static void Tabb(double min, double max, double step){
        while (min <= max){
            System.out.println(Math.sin(min));
            min += step;
        }
    }


    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Введите минимум");
        double min = in.nextDouble();
        System.out.println("Введите максимум");
        double max = in.nextDouble();
        while (min > max){
            System.out.println("Минимум должен быть меньше максимума");
            System.out.println("Введите минимум");
            min = in.nextDouble();
            System.out.println("Введите максимум");
            max = in.nextDouble();
        }
        System.out.println("Введите шаг");
        double step = in.nextDouble();
        while (step <= 0){
            System.out.println("Шаг должен быть положительным. Введите заново");
            step = in.nextDouble();
        }

        Tabb(min, max, step);
    }
}
