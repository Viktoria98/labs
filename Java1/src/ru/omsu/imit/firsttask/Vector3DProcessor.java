package ru.omsu.imit.firsttask;

public class Vector3DProcessor {

    public static Vector3D addition(Vector3D vector1, Vector3D vector2){
        Vector3D sum = new Vector3D(vector1.getX() + vector2.getX(), vector1.getY() + vector2.getY(), vector1.getZ() + vector2.getZ());
        return sum;
    }

    public static Vector3D subtraction(Vector3D vector1, Vector3D vector2){
        Vector3D sub = new Vector3D(vector1.getX() - vector2.getX(), vector1.getY() - vector2.getY(), vector1.getZ() - vector2.getZ());
        return sub;
    }

    public static double dotProduct(Vector3D vector1, Vector3D vector2){
        return vector1.getX()*vector2.getX() + vector1.getY()*vector2.getY() + vector1.getZ()*vector2.getZ();
    }

    public static Vector3D crossProduct(Vector3D vector1, Vector3D vector2){
        Vector3D cp = new Vector3D(vector1.getY()*vector2.getZ() - vector1.getZ()*vector2.getY(),
                vector1.getZ()*vector2.getX() - vector1.getX()*vector2.getZ(),
                vector1.getX()*vector2.getY() - vector1.getY()*vector2.getX());
        return cp;
    }

    public static boolean isCollinear(Vector3D vector1, Vector3D vector2){
        return vector1.getX() * vector2.getY() == vector1.getY() * vector2.getX() &&
                vector1.getY()/vector2.getZ() == vector1.getZ()/vector2.getY();
    }

    public static void main(String[] args) {
        Vector3D vect1 = new Vector3D(1,1,0);
        Vector3D vect2 = new Vector3D(2,2,0);
        isCollinear(vect1, vect2);
    }
}
