package filter;

public class BeginStringFilter implements Filter{
    private String pattern;

    public BeginStringFilter(String pattern){
        if(pattern == null || pattern.isEmpty()){
            throw new IllegalArgumentException();
        }
        this.pattern = pattern;
    }


    @Override
    public boolean apply(String string) {
        return string != null && string.startsWith(pattern);
    }
}
