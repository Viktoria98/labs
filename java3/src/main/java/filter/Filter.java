package filter;

public interface Filter {
    boolean apply(String string);
}
