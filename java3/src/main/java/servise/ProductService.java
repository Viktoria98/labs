package servise;

import filter.Filter;
import products.Consigment;

public class ProductService {
    public static int countByFilter(Filter filter, Consigment consigment){
        int count = 0;
        for(int i=0;i<consigment.getCoverables().length;i++){
            if(filter.apply(consigment.getCoverables()[i].getName())){
                count++;
            }
        }
        return count;
    }
}
